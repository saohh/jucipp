#pragma once
#include <condition_variable>
#include <functional>
#include <list>
#include <thread>
#include <vector>

class Workers {
  size_t worker_count;
  std::vector<std::thread> threads;

  std::mutex start_stop_mutex, mutex;

  std::list<std::function<void()>> tasks;
  std::condition_variable cv;

  bool stop_threads = false;
  bool stop_threads_on_completed_tasks = false;

public:
  /// Calls start().
  Workers(size_t worker_count = 1);

  /// Calls stop().
  ~Workers();

  /// Will be called by constructor. For use after stop() only.
  void start();

  /// Add task to be processed by worker(s).
  void post(std::function<void()> &&task);

  /// Stop threads when tasks have been completed. Waits until all of the threads have completed.
  void stop();

  /// Calls stop() and start().
  void restart();
};
