#!/bin/sh
# Based on https://stackoverflow.com/a/8375093
# TODO: Rez, DeRez and SetFile are deprecated, but I have not yet found replacement tools
# Sets an icon on file or directory
# Usage setIcon.sh iconimage.jpg /path/to/[file|folder]
iconSource=$1
iconDestination=$2
icon=/tmp/`basename $iconSource`
rsrc=/tmp/juci.rsrc

# Create icon from the iconSource
cp $iconSource $icon

# Add icon to image file, meaning use itself as the icon
sips -i $icon > /dev/null

# Take that icon and put it into a rsrc file
DeRez -only icns $icon > $rsrc

# Apply the rsrc file to
SetFile -a C $iconDestination

# Append resource to the file you want to icon-ize.
Rez -append $rsrc -o $iconDestination

rm $rsrc $icon
