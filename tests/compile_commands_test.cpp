#include "compile_commands.hpp"
#include <glib.h>
#include <gtkmm.h>
#include <gtksourceviewmm.h>

int main() {
  auto app = Gtk::Application::create();
  Gsv::init();

  auto tests_path = boost::filesystem::canonical(JUCI_TESTS_PATH);

  {
    CompileCommands compile_commands(tests_path / "meson_old_test_files" / "build");

    g_assert(compile_commands.commands.at(0).directory == "jucipp/tests/meson_old_test_files/build");

    g_assert_cmpuint(compile_commands.commands.size(), ==, 4);

    auto argument_values = compile_commands.commands.at(0).get_argument_values("-o");
    g_assert_cmpuint(argument_values.size(), ==, 1);
    g_assert_cmpstr(argument_values.at(0).c_str(), ==, "hello_lib@sta/main.cpp.o");

    g_assert(boost::filesystem::canonical(compile_commands.commands.at(0).file) == tests_path / "meson_old_test_files" / "main.cpp");
  }

  {
    CompileCommands compile_commands(tests_path / "source_clang_test_files" / "build");

    g_assert(compile_commands.commands.at(0).directory == ".");

    g_assert_cmpuint(compile_commands.commands.size(), ==, 1);

    g_assert_cmpstr(compile_commands.commands.at(0).arguments.at(3).c_str(), ==, "-Wall");
  }
}
